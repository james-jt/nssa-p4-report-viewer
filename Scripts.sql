
/*Script to insert "Retirement" and "Death" into Termination Reasons Table*/
INSERT INTO TERMINATIONRESONS
VALUES (5, 'Retirement', '', 'Admin');

INSERT INTO TERMINATIONRESONS
VALUES (6, 'Death', '', 'Admin');

/*Script to alter EMPPERSONALDETAILS Table to include "Nature of Employment" field.*/
ALTER TABLE EMPPERSONALDETAILS
	ADD NATUREOFEMPLOYMENT CHAR;

/*Scripts to add "SSN" and "Nature of Employment" details into table EMPPERSONALDETAILS */
UPDATE EMPPERSONALDETAILS SET EMPSSNUMBER = '231284' 		, NATUREOFEMPLOYMENT = 'X'      					WHERE EMPNUMBER = 'BK001'	;

 