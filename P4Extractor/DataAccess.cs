﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirebirdSql.Data.FirebirdClient;
using System.Configuration;
using System.Data;
using System.Windows.Forms;

namespace P4Extractor
{
    public class DataAccess
    {
        public string connectionString { get; set; }
        FbConnection cnn;
        string sqlStr;
        FbCommand cmd;

        public DataAccess()
        {
            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public DataSet ObtainPayrollTypes()
        {
            sqlStr = @"SELECT CODE, DESCRIPTION FROM PAYROLLTYPE WHERE CODE <> '**N/D**'";
            return ExecuteQuery();
        }

        public DataSet ObtainPayrollNumbers(string payrollType)
        {
            sqlStr = string.Format("SELECT DISTINCT PAYROLLNUMBER FROM PAYROLLOUTPUT WHERE PAYROLLTYPE = '{0}'", payrollType);
            return ExecuteQuery();
        }

        public DataSet ObtainPayrollMonthAndYear(string payrollNumber)
        {
            sqlStr = string.Format("SELECT P.PAYROLLSEQUENCE, F.FINANCIALYEAR FROM PAYROLL P, FINANCIALPERIODS F WHERE P.PAYROLLNUMBER = '{0}' AND P.FINANCIALPERIOD = F.CODE;", payrollNumber);
            return ExecuteQuery();
        }

        public DataSet ObtainFinancialYear()
        {
            sqlStr = @" SELECT F.FINANCIALYEAR 
                        FROM PAYROLL P, FINANCIALPERIODS F 
                        WHERE P.FINANCIALPERIOD IN (SELECT DISTINCT FINANCIALPERIOD FROM PAYROLL) AND 
                        P.FINANCIALPERIOD = F.CODE;";
            return ExecuteQuery();
        }

        public DataSet ObtainPayDate(string payrollType, string payrollNumber)
        {
            sqlStr = string.Format("SELECT DISTINCT PAYDATE FROM PAYROLLOUTPUT WHERE PAYROLLTYPE = '{0}' AND PAYROLLNUMBER = '{1}'", payrollType, payrollNumber);
            return ExecuteQuery(); 
        }

        public DataSet GetReportData(string payrollType, string payrollNumber, string NssaDeductionCode)
        {
            sqlStr = string.Format(@"SELECT	E.EMPSSNUMBER, 
		                    E.EMPNUMBER, 
                            E.IDNUMBER, 
                            E.DATEOFBIRTH, 
                            E.SURNAME, 
                            E.NAME, 
		                    E.DATEOFHIRE, 
                            E.DATELEFT, 
                            E.TERMINATIONREASON, 
                            E.NATUREOFEMPLOYMENT,
                            O.BASICPAY, /*This figure will be recalculated to incorporate the insurable earnings ceiling for its value*/
                            2*D.AMOUNT, 
                            O.BASICPAY
                    FROM    EMPPERSONALDETAILS E, PAYROLLOUTPUT O, OUTDEDUCTIONS D 
                    WHERE   O.EMPNUMBER = E.EMPNUMBER AND
                            D.EMPNUMBER = O.EMPNUMBER AND
                            O.PAYROLLNUMBER = '{0}' AND
                            D.PAYROLLNUMBER = O.PAYROLLNUMBER AND
                            O.PAYROLLTYPE = '{1}' AND
                            D.PAYROLLTYPE = O.PAYROLLTYPE AND
                            D.DEDUCTION = '{2}'", payrollNumber, payrollType, NssaDeductionCode);
            return ExecuteQuery();
        }

        public DataSet GetCombinedReportData(string payrollSequence, string NssaDeductionCode)
        { 
            sqlStr = string.Format(@"SELECT	E.EMPSSNUMBER, 
		                    E.EMPNUMBER, 
                            E.IDNUMBER, 
                            E.DATEOFBIRTH, 
                            E.SURNAME, 
                            E.NAME, 
		                    E.DATEOFHIRE, 
                            E.DATELEFT, 
                            E.TERMINATIONREASON, 
                            E.NATUREOFEMPLOYMENT,
                            O.BASICPAY, /*This figure will be recalculated to incorporate the insurable earnings ceiling for its value*/
                            2*D.AMOUNT, 
                            O.BASICPAY
                    FROM    EMPPERSONALDETAILS E, PAYROLLOUTPUT O, OUTDEDUCTIONS D 
                    WHERE   O.EMPNUMBER = E.EMPNUMBER AND
                            D.EMPNUMBER = O.EMPNUMBER AND
                            O.PAYROLLNUMBER IN        
                                                (SELECT PAYROLLNUMBER 
                                                FROM PAYROLL 
                                                WHERE PAYROLLSEQUENCE = '{0}') 
                                            AND
                            D.PAYROLLNUMBER = O.PAYROLLNUMBER AND
                            D.DEDUCTION = '{1}'", payrollSequence, NssaDeductionCode);
            return ExecuteQuery();
        }

        public DataSet GetAllEmployees()
        {
            sqlStr = @"SELECT	EMPNUMBER, 
                                DATEOFBIRTH, 
                                SURNAME, 
                                NAME,
                                NATUREOFEMPLOYMENT 
                        FROM    EMPPERSONALDETAILS";
            return ExecuteQuery();
        }

        public bool UpdateEmployeesNOE(DataTable dt)
        {
            bool flag = false;
            for (int row = 0; row < dt.Rows.Count; row++)
            {
                sqlStr = string.Format(@"UPDATE EMPPERSONALDETAILS SET NATUREOFEMPLOYMENT = '{0}' WHERE EMPNUMBER = '{1}';", dt.Rows[row]["Current Nature of Employment"], dt.Rows[row]["Employee Number"]);
                flag = ExecuteNonQuery();
            }
            return flag;
        }
         
        #region Helper Methods

        private bool OpenConnection()
        {
            try
            {
                this.cnn = new FbConnection(connectionString);
                this.cnn.Open();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private DataSet ExecuteQuery()
        {
            var ds = new DataSet();
            if (OpenConnection())
            {
                try
                {
                    this.cmd = new FbCommand(sqlStr, this.cnn);
                    this.cmd.CommandType = CommandType.Text;
                    var da = new FbDataAdapter(this.cmd);
                    da.Fill(ds);
                    cnn.Close();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            return ds;
        }

        private bool ExecuteNonQuery()
        {
            if (OpenConnection())
            {
                try
                {
                    this.cmd = new FbCommand(sqlStr, this.cnn);
                    this.cmd.CommandType = CommandType.Text;
                    this.cmd.ExecuteNonQuery();
                    cnn.Close();
                    return true;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return false;
        }

        #endregion
    }
}
