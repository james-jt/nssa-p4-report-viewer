﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Legacy Payroll - P4 Report (New Format) Viewer")]
[assembly: AssemblyDescription("NSSA P4 Report (New Format) Viewer for Legacy Payroll.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("James Taruvinga")]
[assembly: AssemblyProduct("Legacy Payroll - P4 Report (New Format) Viewer")]
[assembly: AssemblyCopyright("Copyright © James Taruvinga")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("dcfae56d-1c06-4cbc-b59b-d55741aad79d")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.0.0")]
[assembly: AssemblyFileVersion("1.1.0.0")]
