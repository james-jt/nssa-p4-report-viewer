﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace P4ReportViewer
{
    public partial class frmNewMapping : Form
    {
        frmMain theMainForm;
        public frmNewMapping(frmMain mainForm)
        {
            InitializeComponent();
            theMainForm = mainForm;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            var fileOpenDialogue = new OpenFileDialog();
            fileOpenDialogue.Filter = "Golden Pay database file (Executive.sci)|Executive.sci";
            fileOpenDialogue.ShowDialog();
            if (string.IsNullOrEmpty(fileOpenDialogue.FileName))
            {
                return;
            }
            string databaseFile = fileOpenDialogue.FileName.ToString();
            txtDatabasePath.Text = databaseFile;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtMappingName.Text))
            {
                MessageBox.Show("Please enter a name for the mapping.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMappingName.Focus();
                return;
            }
            if (txtMappingName.Text.Contains("\"") | txtMappingName.Text.Contains("<") | txtMappingName.Text.Contains(">") | txtMappingName.Text.Contains("/"))
            {
                MessageBox.Show("Invalid characters found. The characters '<', '>', '\"' and '/' are not permitted.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMappingName.Focus();
                txtMappingName.SelectAll();
                return;
            }
            if (string.IsNullOrEmpty(txtDatabasePath.Text))
            {
                MessageBox.Show("Please specify location of database file.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (CreateNewMapping())
            {
                string msg = string.Format("New mapping [{0}] successfully created.", txtMappingName.Text);
                MessageBox.Show(msg, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        #region Helper Methods

        private bool CreateNewMapping() 
        {
            string newMappingName = txtMappingName.Text;
            string newMappingConnectionString = string.Format(@"database = localhost:{0};
            User = SYSDBA; Password = masterkey; DataSource = localhost; Port = 3050; Dialect = 3; Charset = NONE;
            Role =; Connection lifetime = 15; Pooling = true; MinPoolSize = 0; MaxPoolSize = 50; Packet Size = 8192; ServerType = 0", txtDatabasePath.Text);
            ConnectionStringSettings newMapping = new ConnectionStringSettings();

            //The '^' character is added to distinguish user-created connection strings and the inbuilt ones. This is useful in displaying user-created mappings only.
            newMapping.Name = "^" + newMappingName;
            newMapping.ConnectionString = newMappingConnectionString;
            theMainForm.SetBusyStatus();
            try
            {
                //Add (the) newMapping to the Mappings collection in the app configuration file.
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings.Add(newMapping);
                
                //Set the new mapping as the default connection string.
                config.ConnectionStrings.ConnectionStrings["DefaultConnection"].ConnectionString = newMappingConnectionString;
                config.AppSettings.Settings["ActiveMapping"].Value = newMappingName;

                //Save configuration
                config.Save(ConfigurationSaveMode.Modified, true);
                ConfigurationManager.RefreshSection("connectionStrings");
                ConfigurationManager.RefreshSection("appSettings");

                theMainForm.SetReadyStatus();
                return true;
            }
            catch (Exception ex)
            {
                theMainForm.SetReadyStatus();
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        #endregion
    }
}
