﻿namespace P4ReportViewer
{
    partial class frmCompanyDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCompanyDetails));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtEmployerRep = new System.Windows.Forms.TextBox();
            this.txtDesignation = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtInsurableEarnings = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPhysicalAddress = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtSector = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtECNo = new System.Windows.Forms.TextBox();
            this.txtICNo = new System.Windows.Forms.TextBox();
            this.txtSSRNo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tspInstructions = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSaveAndContinue = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSaveAndClose = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Controls.Add(this.statusStrip1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 62);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(834, 466);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtEmployerRep);
            this.panel1.Controls.Add(this.txtDesignation);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtInsurableEarnings);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtCompanyName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtPhysicalAddress);
            this.panel1.Controls.Add(this.txtEmail);
            this.panel1.Controls.Add(this.txtSector);
            this.panel1.Controls.Add(this.txtPhone);
            this.panel1.Controls.Add(this.txtECNo);
            this.panel1.Controls.Add(this.txtICNo);
            this.panel1.Controls.Add(this.txtSSRNo);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(828, 422);
            this.panel1.TabIndex = 6;
            // 
            // txtEmployerRep
            // 
            this.txtEmployerRep.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtEmployerRep.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmployerRep.Location = new System.Drawing.Point(135, 303);
            this.txtEmployerRep.Name = "txtEmployerRep";
            this.txtEmployerRep.Size = new System.Drawing.Size(157, 16);
            this.txtEmployerRep.TabIndex = 9;
            this.txtEmployerRep.Leave += new System.EventHandler(this.txtEmployerRep_Leave);
            // 
            // txtDesignation
            // 
            this.txtDesignation.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtDesignation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDesignation.Location = new System.Drawing.Point(135, 339);
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.Size = new System.Drawing.Size(157, 16);
            this.txtDesignation.TabIndex = 10;
            this.txtDesignation.Leave += new System.EventHandler(this.txtDesignation_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 340);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 15);
            this.label7.TabIndex = 19;
            this.label7.Text = "Designation :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 304);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 15);
            this.label11.TabIndex = 18;
            this.label11.Text = "Employer Rep :";
            // 
            // txtInsurableEarnings
            // 
            this.txtInsurableEarnings.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtInsurableEarnings.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInsurableEarnings.Location = new System.Drawing.Point(135, 375);
            this.txtInsurableEarnings.Name = "txtInsurableEarnings";
            this.txtInsurableEarnings.Size = new System.Drawing.Size(157, 16);
            this.txtInsurableEarnings.TabIndex = 11;
            this.txtInsurableEarnings.Leave += new System.EventHandler(this.txtInsurableEarnings_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 376);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 15);
            this.label2.TabIndex = 16;
            this.label2.Text = "Insurable Earnings :";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCompanyName.Location = new System.Drawing.Point(135, 15);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(610, 16);
            this.txtCompanyName.TabIndex = 1;
            this.txtCompanyName.Leave += new System.EventHandler(this.txtCompanyName_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 15);
            this.label1.TabIndex = 14;
            this.label1.Text = "Company Name :";
            // 
            // txtPhysicalAddress
            // 
            this.txtPhysicalAddress.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtPhysicalAddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhysicalAddress.Location = new System.Drawing.Point(135, 267);
            this.txtPhysicalAddress.Name = "txtPhysicalAddress";
            this.txtPhysicalAddress.Size = new System.Drawing.Size(610, 16);
            this.txtPhysicalAddress.TabIndex = 8;
            this.txtPhysicalAddress.Leave += new System.EventHandler(this.txtPhysicalAddress_Leave);
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail.Location = new System.Drawing.Point(135, 195);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(157, 16);
            this.txtEmail.TabIndex = 6;
            this.txtEmail.Leave += new System.EventHandler(this.txtEmail_Leave);
            // 
            // txtSector
            // 
            this.txtSector.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtSector.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSector.Location = new System.Drawing.Point(135, 159);
            this.txtSector.Name = "txtSector";
            this.txtSector.Size = new System.Drawing.Size(157, 16);
            this.txtSector.TabIndex = 5;
            this.txtSector.Leave += new System.EventHandler(this.txtSector_Leave);
            // 
            // txtPhone
            // 
            this.txtPhone.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtPhone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhone.Location = new System.Drawing.Point(135, 231);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(157, 16);
            this.txtPhone.TabIndex = 7;
            this.txtPhone.Leave += new System.EventHandler(this.txtPhone_Leave);
            // 
            // txtECNo
            // 
            this.txtECNo.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtECNo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtECNo.Location = new System.Drawing.Point(135, 87);
            this.txtECNo.Name = "txtECNo";
            this.txtECNo.Size = new System.Drawing.Size(157, 16);
            this.txtECNo.TabIndex = 3;
            this.txtECNo.Leave += new System.EventHandler(this.txtECNo_Leave);
            // 
            // txtICNo
            // 
            this.txtICNo.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtICNo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtICNo.Location = new System.Drawing.Point(135, 123);
            this.txtICNo.Name = "txtICNo";
            this.txtICNo.Size = new System.Drawing.Size(157, 16);
            this.txtICNo.TabIndex = 4;
            this.txtICNo.Leave += new System.EventHandler(this.txtICNo_Leave);
            // 
            // txtSSRNo
            // 
            this.txtSSRNo.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtSSRNo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSSRNo.Location = new System.Drawing.Point(135, 51);
            this.txtSSRNo.Name = "txtSSRNo";
            this.txtSSRNo.Size = new System.Drawing.Size(157, 16);
            this.txtSSRNo.TabIndex = 2;
            this.txtSSRNo.Leave += new System.EventHandler(this.txtSSRNo_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 268);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 15);
            this.label8.TabIndex = 6;
            this.label8.Text = "Physical Address :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 232);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 15);
            this.label9.TabIndex = 5;
            this.label9.Text = "Phone :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 196);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 15);
            this.label10.TabIndex = 4;
            this.label10.Text = "Email :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 15);
            this.label6.TabIndex = 3;
            this.label6.Text = "Sector :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 15);
            this.label5.TabIndex = 2;
            this.label5.Text = "Employer IC :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "Employer EC :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Employer SSR No. :";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tspInstructions});
            this.statusStrip1.Location = new System.Drawing.Point(3, 441);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip1.Size = new System.Drawing.Size(828, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tspInstructions
            // 
            this.tspInstructions.Name = "tspInstructions";
            this.tspInstructions.Size = new System.Drawing.Size(425, 17);
            this.tspInstructions.Text = "Ammend company details as per requirement and save. Blanks are not allowed.";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(834, 62);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSaveAndContinue);
            this.panel2.Controls.Add(this.btnCancel);
            this.panel2.Controls.Add(this.btnSaveAndClose);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(373, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(458, 40);
            this.panel2.TabIndex = 4;
            // 
            // btnSaveAndContinue
            // 
            this.btnSaveAndContinue.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnSaveAndContinue.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnSaveAndContinue.Location = new System.Drawing.Point(27, 2);
            this.btnSaveAndContinue.Name = "btnSaveAndContinue";
            this.btnSaveAndContinue.Size = new System.Drawing.Size(136, 31);
            this.btnSaveAndContinue.TabIndex = 12;
            this.btnSaveAndContinue.Text = "Save and Continue";
            this.btnSaveAndContinue.UseVisualStyleBackColor = true;
            this.btnSaveAndContinue.Click += new System.EventHandler(this.btnSaveAndContinue_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnCancel.Location = new System.Drawing.Point(316, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(136, 31);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSaveAndClose
            // 
            this.btnSaveAndClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnSaveAndClose.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnSaveAndClose.Location = new System.Drawing.Point(172, 2);
            this.btnSaveAndClose.Name = "btnSaveAndClose";
            this.btnSaveAndClose.Size = new System.Drawing.Size(136, 31);
            this.btnSaveAndClose.TabIndex = 13;
            this.btnSaveAndClose.Text = "Save and Close";
            this.btnSaveAndClose.UseVisualStyleBackColor = true;
            this.btnSaveAndClose.Click += new System.EventHandler(this.btnSaveAndClose_Click);
            // 
            // frmCompanyDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(834, 528);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(800, 550);
            this.Name = "frmCompanyDetails";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Company Details";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tspInstructions;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSaveAndContinue;
        private System.Windows.Forms.Button btnSaveAndClose;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtPhysicalAddress;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtSector;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtECNo;
        private System.Windows.Forms.TextBox txtICNo;
        private System.Windows.Forms.TextBox txtSSRNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInsurableEarnings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEmployerRep;
        private System.Windows.Forms.TextBox txtDesignation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
    }
}