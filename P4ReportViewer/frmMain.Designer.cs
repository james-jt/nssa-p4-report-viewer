﻿namespace P4ReportViewer
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allowEditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disallowEditsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectMappingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newMappingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewDatabaseInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.captureNatureOfEmploymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editCompanyDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.lblCompanyName = new System.Windows.Forms.ToolStripLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnViewAll = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbPayMonth = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.cmbPayrollNumber = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtPayDate = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtMonthAndYear = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtEmployerRep = new System.Windows.Forms.TextBox();
            this.txtDesignation = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPhysicalAddress = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtSector = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtECNo = new System.Windows.Forms.TextBox();
            this.txtICNo = new System.Windows.Forms.TextBox();
            this.txtSSRNo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbPayrollType = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvP4Report = new System.Windows.Forms.DataGridView();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.picReadyStatus = new System.Windows.Forms.ToolStripButton();
            this.picBusyStatus = new System.Windows.Forms.ToolStripButton();
            this.lblStatus = new System.Windows.Forms.ToolStripLabel();
            this.lblMappingInfo = new System.Windows.Forms.ToolStripLabel();
            this.menuStrip1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvP4Report)).BeginInit();
            this.toolStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.databaseToolStripMenuItem,
            this.employeesToolStripMenuItem,
            this.companyToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1038, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.exportToolStripMenuItem.Text = "&Export...";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allowEditToolStripMenuItem,
            this.disallowEditsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            this.editToolStripMenuItem.Visible = false;
            // 
            // allowEditToolStripMenuItem
            // 
            this.allowEditToolStripMenuItem.Name = "allowEditToolStripMenuItem";
            this.allowEditToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.allowEditToolStripMenuItem.Text = "Allow Edits";
            // 
            // disallowEditsToolStripMenuItem
            // 
            this.disallowEditsToolStripMenuItem.Name = "disallowEditsToolStripMenuItem";
            this.disallowEditsToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.disallowEditsToolStripMenuItem.Text = "Disallow Edits";
            // 
            // databaseToolStripMenuItem
            // 
            this.databaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectMappingToolStripMenuItem,
            this.newMappingToolStripMenuItem,
            this.viewDatabaseInfoToolStripMenuItem});
            this.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            this.databaseToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.databaseToolStripMenuItem.Text = "&Database";
            // 
            // selectMappingToolStripMenuItem
            // 
            this.selectMappingToolStripMenuItem.Name = "selectMappingToolStripMenuItem";
            this.selectMappingToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.selectMappingToolStripMenuItem.Text = "&Select Mapping";
            // 
            // newMappingToolStripMenuItem
            // 
            this.newMappingToolStripMenuItem.Name = "newMappingToolStripMenuItem";
            this.newMappingToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.newMappingToolStripMenuItem.Text = "&New Mapping...";
            this.newMappingToolStripMenuItem.Click += new System.EventHandler(this.newMappingToolStripMenuItem_Click);
            // 
            // viewDatabaseInfoToolStripMenuItem
            // 
            this.viewDatabaseInfoToolStripMenuItem.Name = "viewDatabaseInfoToolStripMenuItem";
            this.viewDatabaseInfoToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.viewDatabaseInfoToolStripMenuItem.Text = "Select &Database...";
            this.viewDatabaseInfoToolStripMenuItem.Click += new System.EventHandler(this.selectDatabaseToolStripMenuItem_Click);
            // 
            // employeesToolStripMenuItem
            // 
            this.employeesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.captureNatureOfEmploymentToolStripMenuItem});
            this.employeesToolStripMenuItem.Name = "employeesToolStripMenuItem";
            this.employeesToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.employeesToolStripMenuItem.Text = "&Employees";
            // 
            // captureNatureOfEmploymentToolStripMenuItem
            // 
            this.captureNatureOfEmploymentToolStripMenuItem.Name = "captureNatureOfEmploymentToolStripMenuItem";
            this.captureNatureOfEmploymentToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.captureNatureOfEmploymentToolStripMenuItem.Text = "Edit &Nature of Employment...";
            this.captureNatureOfEmploymentToolStripMenuItem.Click += new System.EventHandler(this.captureNatureOfEmploymentToolStripMenuItem_Click);
            // 
            // companyToolStripMenuItem
            // 
            this.companyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editCompanyDetailsToolStripMenuItem});
            this.companyToolStripMenuItem.Name = "companyToolStripMenuItem";
            this.companyToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.companyToolStripMenuItem.Text = "&Company";
            // 
            // editCompanyDetailsToolStripMenuItem
            // 
            this.editCompanyDetailsToolStripMenuItem.Name = "editCompanyDetailsToolStripMenuItem";
            this.editCompanyDetailsToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.editCompanyDetailsToolStripMenuItem.Text = "Edit Company &Details...";
            this.editCompanyDetailsToolStripMenuItem.Click += new System.EventHandler(this.editCompanyDetailsToolStripMenuItem_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.searchToolStripMenuItem.Text = "&Search";
            this.searchToolStripMenuItem.Visible = false;
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewHelpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // viewHelpToolStripMenuItem
            // 
            this.viewHelpToolStripMenuItem.Name = "viewHelpToolStripMenuItem";
            this.viewHelpToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.viewHelpToolStripMenuItem.Text = "&View Help...";
            this.viewHelpToolStripMenuItem.Click += new System.EventHandler(this.viewHelpToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1038, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblCompanyName});
            this.toolStrip2.Location = new System.Drawing.Point(0, 49);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.Size = new System.Drawing.Size(1038, 25);
            this.toolStrip2.TabIndex = 5;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.lblCompanyName.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(0, 22);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel4);
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.btnViewAll);
            this.groupBox1.Controls.Add(this.btnView);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbPayMonth);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.btnExport);
            this.groupBox1.Controls.Add(this.cmbPayrollNumber);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.cmbPayrollType);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 74);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1038, 246);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel4.Location = new System.Drawing.Point(12, 202);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(323, 1);
            this.panel4.TabIndex = 15;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel3.Location = new System.Drawing.Point(12, 149);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(323, 1);
            this.panel3.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel2.Location = new System.Drawing.Point(12, 31);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(323, 1);
            this.panel2.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label16.Location = new System.Drawing.Point(10, 132);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(125, 15);
            this.label16.TabIndex = 13;
            this.label16.Text = "View all Payroll Types :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label15.Location = new System.Drawing.Point(10, 14);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(121, 15);
            this.label15.TabIndex = 12;
            this.label15.Text = "View by Payroll Type :";
            // 
            // btnViewAll
            // 
            this.btnViewAll.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnViewAll.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnViewAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnViewAll.Location = new System.Drawing.Point(229, 157);
            this.btnViewAll.Name = "btnViewAll";
            this.btnViewAll.Size = new System.Drawing.Size(104, 27);
            this.btnViewAll.TabIndex = 5;
            this.btnViewAll.Text = "View";
            this.btnViewAll.UseVisualStyleBackColor = true;
            this.btnViewAll.Click += new System.EventHandler(this.btnViewAll_Click);
            // 
            // btnView
            // 
            this.btnView.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnView.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnView.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnView.Location = new System.Drawing.Point(118, 99);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(215, 27);
            this.btnView.TabIndex = 3;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(16, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Payroll Number:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(16, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Payroll Type:";
            // 
            // cmbPayMonth
            // 
            this.cmbPayMonth.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.cmbPayMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPayMonth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPayMonth.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPayMonth.FormattingEnabled = true;
            this.cmbPayMonth.Items.AddRange(new object[] {
            "Bonus",
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cmbPayMonth.Location = new System.Drawing.Point(118, 159);
            this.cmbPayMonth.Name = "cmbPayMonth";
            this.cmbPayMonth.Size = new System.Drawing.Size(104, 23);
            this.cmbPayMonth.TabIndex = 4;
            this.cmbPayMonth.SelectedIndexChanged += new System.EventHandler(this.cmbPayMonth_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 162);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 15);
            this.label12.TabIndex = 0;
            this.label12.Text = "Pay Month :";
            // 
            // btnExport
            // 
            this.btnExport.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnExport.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnExport.Location = new System.Drawing.Point(118, 209);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(215, 27);
            this.btnExport.TabIndex = 6;
            this.btnExport.Text = "Export...";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // cmbPayrollNumber
            // 
            this.cmbPayrollNumber.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.cmbPayrollNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPayrollNumber.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPayrollNumber.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPayrollNumber.FormattingEnabled = true;
            this.cmbPayrollNumber.Location = new System.Drawing.Point(118, 69);
            this.cmbPayrollNumber.Name = "cmbPayrollNumber";
            this.cmbPayrollNumber.Size = new System.Drawing.Size(234, 23);
            this.cmbPayrollNumber.TabIndex = 2;
            this.cmbPayrollNumber.SelectedIndexChanged += new System.EventHandler(this.cmbPayrollNumber_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtPayDate);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtMonthAndYear);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txtEmployerRep);
            this.panel1.Controls.Add(this.txtDesignation);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtPhysicalAddress);
            this.panel1.Controls.Add(this.txtEmail);
            this.panel1.Controls.Add(this.txtSector);
            this.panel1.Controls.Add(this.txtPhone);
            this.panel1.Controls.Add(this.txtECNo);
            this.panel1.Controls.Add(this.txtICNo);
            this.panel1.Controls.Add(this.txtSSRNo);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(387, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(648, 224);
            this.panel1.TabIndex = 5;
            // 
            // txtPayDate
            // 
            this.txtPayDate.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtPayDate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPayDate.Location = new System.Drawing.Point(425, 171);
            this.txtPayDate.Name = "txtPayDate";
            this.txtPayDate.ReadOnly = true;
            this.txtPayDate.Size = new System.Drawing.Size(211, 16);
            this.txtPayDate.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(339, 171);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 15);
            this.label14.TabIndex = 16;
            this.label14.Text = "Pay Date : ";
            // 
            // txtMonthAndYear
            // 
            this.txtMonthAndYear.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtMonthAndYear.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMonthAndYear.Location = new System.Drawing.Point(116, 171);
            this.txtMonthAndYear.Name = "txtMonthAndYear";
            this.txtMonthAndYear.ReadOnly = true;
            this.txtMonthAndYear.Size = new System.Drawing.Size(211, 16);
            this.txtMonthAndYear.TabIndex = 16;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 171);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 15);
            this.label13.TabIndex = 8;
            this.label13.Text = "Month and Year :";
            // 
            // txtEmployerRep
            // 
            this.txtEmployerRep.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtEmployerRep.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmployerRep.Location = new System.Drawing.Point(116, 138);
            this.txtEmployerRep.Name = "txtEmployerRep";
            this.txtEmployerRep.ReadOnly = true;
            this.txtEmployerRep.Size = new System.Drawing.Size(211, 16);
            this.txtEmployerRep.TabIndex = 14;
            // 
            // txtDesignation
            // 
            this.txtDesignation.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtDesignation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDesignation.Location = new System.Drawing.Point(425, 138);
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.ReadOnly = true;
            this.txtDesignation.Size = new System.Drawing.Size(211, 16);
            this.txtDesignation.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(339, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 15);
            this.label7.TabIndex = 13;
            this.label7.Text = "Designation :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 138);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 15);
            this.label11.TabIndex = 12;
            this.label11.Text = "Employer Rep :";
            // 
            // txtPhysicalAddress
            // 
            this.txtPhysicalAddress.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtPhysicalAddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhysicalAddress.Location = new System.Drawing.Point(116, 105);
            this.txtPhysicalAddress.Name = "txtPhysicalAddress";
            this.txtPhysicalAddress.ReadOnly = true;
            this.txtPhysicalAddress.Size = new System.Drawing.Size(520, 16);
            this.txtPhysicalAddress.TabIndex = 13;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail.Location = new System.Drawing.Point(116, 72);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.ReadOnly = true;
            this.txtEmail.Size = new System.Drawing.Size(211, 16);
            this.txtEmail.TabIndex = 11;
            // 
            // txtSector
            // 
            this.txtSector.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtSector.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSector.Location = new System.Drawing.Point(425, 39);
            this.txtSector.Name = "txtSector";
            this.txtSector.ReadOnly = true;
            this.txtSector.Size = new System.Drawing.Size(211, 16);
            this.txtSector.TabIndex = 10;
            // 
            // txtPhone
            // 
            this.txtPhone.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtPhone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhone.Location = new System.Drawing.Point(425, 72);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.ReadOnly = true;
            this.txtPhone.Size = new System.Drawing.Size(211, 16);
            this.txtPhone.TabIndex = 12;
            // 
            // txtECNo
            // 
            this.txtECNo.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtECNo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtECNo.Location = new System.Drawing.Point(425, 6);
            this.txtECNo.Name = "txtECNo";
            this.txtECNo.ReadOnly = true;
            this.txtECNo.Size = new System.Drawing.Size(211, 16);
            this.txtECNo.TabIndex = 8;
            // 
            // txtICNo
            // 
            this.txtICNo.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtICNo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtICNo.Location = new System.Drawing.Point(116, 39);
            this.txtICNo.Name = "txtICNo";
            this.txtICNo.ReadOnly = true;
            this.txtICNo.Size = new System.Drawing.Size(211, 16);
            this.txtICNo.TabIndex = 9;
            // 
            // txtSSRNo
            // 
            this.txtSSRNo.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtSSRNo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSSRNo.Location = new System.Drawing.Point(116, 6);
            this.txtSSRNo.Name = "txtSSRNo";
            this.txtSSRNo.ReadOnly = true;
            this.txtSSRNo.Size = new System.Drawing.Size(211, 16);
            this.txtSSRNo.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 15);
            this.label8.TabIndex = 6;
            this.label8.Text = "Physical Address :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(339, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 15);
            this.label9.TabIndex = 5;
            this.label9.Text = "Phone :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 15);
            this.label10.TabIndex = 4;
            this.label10.Text = "Email :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(339, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 15);
            this.label6.TabIndex = 3;
            this.label6.Text = "Sector :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 15);
            this.label5.TabIndex = 2;
            this.label5.Text = "Employer IC :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(339, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "Employer EC :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Employer SSR No. :";
            // 
            // cmbPayrollType
            // 
            this.cmbPayrollType.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.cmbPayrollType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPayrollType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPayrollType.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPayrollType.FormattingEnabled = true;
            this.cmbPayrollType.Location = new System.Drawing.Point(118, 38);
            this.cmbPayrollType.Name = "cmbPayrollType";
            this.cmbPayrollType.Size = new System.Drawing.Size(234, 23);
            this.cmbPayrollType.TabIndex = 1;
            this.cmbPayrollType.SelectedIndexChanged += new System.EventHandler(this.cmbPayrollType_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvP4Report);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 320);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1038, 270);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // dgvP4Report
            // 
            this.dgvP4Report.AllowUserToAddRows = false;
            this.dgvP4Report.AllowUserToDeleteRows = false;
            this.dgvP4Report.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.dgvP4Report.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvP4Report.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvP4Report.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvP4Report.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvP4Report.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvP4Report.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dgvP4Report.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvP4Report.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.MenuHighlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvP4Report.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvP4Report.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvP4Report.Location = new System.Drawing.Point(3, 19);
            this.dgvP4Report.Name = "dgvP4Report";
            this.dgvP4Report.ReadOnly = true;
            this.dgvP4Report.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvP4Report.RowHeadersVisible = false;
            this.dgvP4Report.Size = new System.Drawing.Size(1032, 248);
            this.dgvP4Report.TabIndex = 0;
            this.dgvP4Report.TabStop = false;
            // 
            // toolStrip3
            // 
            this.toolStrip3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.toolStrip3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.picReadyStatus,
            this.picBusyStatus,
            this.lblStatus,
            this.lblMappingInfo});
            this.toolStrip3.Location = new System.Drawing.Point(0, 565);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip3.ShowItemToolTips = false;
            this.toolStrip3.Size = new System.Drawing.Size(1038, 25);
            this.toolStrip3.TabIndex = 8;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // picReadyStatus
            // 
            this.picReadyStatus.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.picReadyStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.picReadyStatus.Image = global::P4ReportViewer.Properties.Resources.green_button_icon_png_13;
            this.picReadyStatus.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.picReadyStatus.Name = "picReadyStatus";
            this.picReadyStatus.Size = new System.Drawing.Size(23, 22);
            this.picReadyStatus.Text = "toolStripButton1";
            // 
            // picBusyStatus
            // 
            this.picBusyStatus.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.picBusyStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.picBusyStatus.Image = global::P4ReportViewer.Properties.Resources.button_153684_960_720;
            this.picBusyStatus.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.picBusyStatus.Name = "picBusyStatus";
            this.picBusyStatus.Size = new System.Drawing.Size(23, 22);
            this.picBusyStatus.Text = "toolStripButton1";
            this.picBusyStatus.Visible = false;
            // 
            // lblStatus
            // 
            this.lblStatus.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(48, 22);
            this.lblStatus.Text = "Ready...";
            // 
            // lblMappingInfo
            // 
            this.lblMappingInfo.Name = "lblMappingInfo";
            this.lblMappingInfo.Size = new System.Drawing.Size(89, 22);
            this.lblMappingInfo.Text = "lblMappingInfo";
            // 
            // frmMain
            // 
            this.AcceptButton = this.btnView;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1038, 590);
            this.Controls.Add(this.toolStrip3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1025, 350);
            this.Name = "frmMain";
            this.Text = "Legacy Payroll - P4 Report (New Format) Viewer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvP4Report)).EndInit();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allowEditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disallowEditsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewDatabaseInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvP4Report;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripLabel lblCompanyName;
        private System.Windows.Forms.ToolStripMenuItem selectMappingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newMappingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem captureNatureOfEmploymentToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSSRNo;
        private System.Windows.Forms.TextBox txtPhysicalAddress;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtSector;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtECNo;
        private System.Windows.Forms.TextBox txtICNo;
        private System.Windows.Forms.ToolStripMenuItem editCompanyDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.TextBox txtEmployerRep;
        private System.Windows.Forms.TextBox txtDesignation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMonthAndYear;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnViewAll;
        private System.Windows.Forms.ComboBox cmbPayMonth;
        private System.Windows.Forms.TextBox txtPayDate;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbPayrollNumber;
        private System.Windows.Forms.ComboBox cmbPayrollType;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripLabel lblStatus;
        private System.Windows.Forms.ToolStripLabel lblMappingInfo;
        private System.Windows.Forms.ToolStripButton picReadyStatus;
        private System.Windows.Forms.ToolStripButton picBusyStatus;
    }
}

