﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace P4ReportViewer
{
    public partial class frmCompanyDetails : Form
    {
        frmMain theMainForm;
         
        public frmCompanyDetails(frmMain mainForm)
        {
            InitializeComponent();
            theMainForm = mainForm;

            if (GetCompanyDetails())
            {
                this.ShowDialog();
            }
            else
            {
                MessageBox.Show("Could not read setup file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void txtCompanyName_Leave(object sender, EventArgs e)
        {
            if (txtCompanyName.Text.Contains("\"") | txtCompanyName.Text.Contains("<") | txtCompanyName.Text.Contains(">") | txtCompanyName.Text.Contains("/"))
            {
                MessageBox.Show("Invalid characters found. The characters '<', '>', '\"' and '/' are not permitted.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCompanyName.Focus();
                txtCompanyName.SelectAll();
                return;
            }
        }

        private void txtSSRNo_Leave(object sender, EventArgs e)
        {
            if (txtSSRNo.Text.Contains("\"") | txtSSRNo.Text.Contains("<") | txtSSRNo.Text.Contains(">") | txtSSRNo.Text.Contains("/"))
            {
                MessageBox.Show("Invalid characters found. The characters '<', '>', '\"' and '/' are not permitted.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtSSRNo.Focus();
                txtSSRNo.SelectAll();
                return;
            }
        }

        private void txtECNo_Leave(object sender, EventArgs e)
        {
            if (txtECNo.Text.Contains("\"") | txtECNo.Text.Contains("<") | txtECNo.Text.Contains(">") | txtECNo.Text.Contains("/"))
            {
                MessageBox.Show("Invalid characters found. The characters '<', '>', '\"' and '/' are not permitted.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtECNo.Focus();
                txtECNo.SelectAll();
                return;
            }
        }

        private void txtICNo_Leave(object sender, EventArgs e)
        {
            if (txtICNo.Text.Contains("\"") | txtICNo.Text.Contains("<") | txtICNo.Text.Contains(">") | txtICNo.Text.Contains("/"))
            {
                MessageBox.Show("Invalid characters found. The characters '<', '>', '\"' and '/' are not permitted.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtICNo.Focus();
                txtICNo.SelectAll();
                return;
            }
        }

        private void txtSector_Leave(object sender, EventArgs e)
        {
            if (txtSector.Text.Contains("\"") | txtSector.Text.Contains("<") | txtSector.Text.Contains(">") | txtSector.Text.Contains("/"))
            {
                MessageBox.Show("Invalid characters found. The characters '<', '>', '\"' and '/' are not permitted.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtSector.Focus();
                txtSector.SelectAll();
                return;
            }
        }

        private void txtEmail_Leave(object sender, EventArgs e)
        {
            if (txtEmail.Text.Contains("\"") | txtEmail.Text.Contains("<") | txtEmail.Text.Contains(">") | txtEmail.Text.Contains("/"))
            {
                MessageBox.Show("Invalid characters found. The characters '<', '>', '\"' and '/' are not permitted.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.Focus();
                txtEmail.SelectAll();
                return;
            }
        }

        private void txtPhone_Leave(object sender, EventArgs e)
        {
            if (txtPhone.Text.Contains("\"") | txtPhone.Text.Contains("<") | txtPhone.Text.Contains(">") | txtPhone.Text.Contains("/"))
            {
                MessageBox.Show("Invalid characters found. The characters '<', '>', '\"' and '/' are not permitted.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPhone.Focus();
                txtPhone.SelectAll();
                return;
            }
        }

        private void txtPhysicalAddress_Leave(object sender, EventArgs e)
        {
            if (txtPhysicalAddress.Text.Contains("\"") | txtPhysicalAddress.Text.Contains("<") | txtPhysicalAddress.Text.Contains(">") | txtPhysicalAddress.Text.Contains("/"))
            {
                MessageBox.Show("Invalid characters found. The characters '<', '>', '\"' and '/' are not permitted.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPhysicalAddress.Focus();
                txtPhysicalAddress.SelectAll();
                return;
            }
        }

        private void txtEmployerRep_Leave(object sender, EventArgs e)
        {
            if (txtEmployerRep.Text.Contains("\"") | txtEmployerRep.Text.Contains("<") | txtEmployerRep.Text.Contains(">") | txtEmployerRep.Text.Contains("/"))
            {
                MessageBox.Show("Invalid characters found. The characters '<', '>', '\"' and '/' are not permitted.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmployerRep.Focus();
                txtEmployerRep.SelectAll();
                return;
            }
        }

        private void txtDesignation_Leave(object sender, EventArgs e)
        {
            if (txtDesignation.Text.Contains("\"") | txtDesignation.Text.Contains("<") | txtDesignation.Text.Contains(">") | txtDesignation.Text.Contains("/"))
            {
                MessageBox.Show("Invalid characters found. The characters '<', '>', '\"' and '/' are not permitted.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtDesignation.Focus();
                txtDesignation.SelectAll();
                return;
            }
        }

        private void txtInsurableEarnings_Leave(object sender, EventArgs e)
        {
            if (txtInsurableEarnings.Text.Contains("\"") | txtInsurableEarnings.Text.Contains("<") | txtInsurableEarnings.Text.Contains(">") | txtInsurableEarnings.Text.Contains("/"))
            {
                MessageBox.Show("Invalid characters found. The characters '<', '>', '\"' and '/' are not permitted.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtInsurableEarnings.Focus();
                txtInsurableEarnings.SelectAll();
                return;
            }
        }

        private void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            if (ValidInput())
            {
                if (UpdateCompanyDetails())
                {
                    this.Close();
                }
            }            
        }

        private void btnSaveAndContinue_Click(object sender, EventArgs e)
        {
            if (ValidInput())
            {
                UpdateCompanyDetails();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Helper Methods

        private bool GetCompanyDetails()
        {
            try
            {
                txtCompanyName.Text = ConfigurationManager.AppSettings["CompanyName"];
                txtSSRNo.Text = ConfigurationManager.AppSettings["SSRNo"];
                txtECNo.Text = ConfigurationManager.AppSettings["ECNo"];
                txtICNo.Text = ConfigurationManager.AppSettings["ICNo"];
                txtSector.Text = ConfigurationManager.AppSettings["Sector"];
                txtEmail.Text = ConfigurationManager.AppSettings["email"];
                txtPhone.Text = ConfigurationManager.AppSettings["Phone"];
                txtPhysicalAddress.Text = ConfigurationManager.AppSettings["Address"];
                txtEmployerRep.Text = ConfigurationManager.AppSettings["EmployerRep"];
                txtDesignation.Text = ConfigurationManager.AppSettings["Designation"];
                txtInsurableEarnings.Text = ConfigurationManager.AppSettings["InsurableEarnings"];
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool ValidInput()
        {
            if (string.IsNullOrEmpty(txtCompanyName.Text))
            {
                MessageBox.Show("Please enter the company name.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCompanyName.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtSSRNo.Text))
            {
                MessageBox.Show("Please enter the employer's SSR Number.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtSSRNo.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtECNo.Text))
            {
                MessageBox.Show("Please enter the employer's EC Number.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtECNo.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtICNo.Text))
            {
                MessageBox.Show("Please enter the employer's IC Number.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtICNo.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtSector.Text))
            {
                MessageBox.Show("Please enter the company's sector.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtSector.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                MessageBox.Show("Please enter the company's email address or N/A if not is available.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtPhone.Text))
            {
                MessageBox.Show("Please enter the company's phone number.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPhone.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtPhysicalAddress.Text))
            {
                MessageBox.Show("Please enter the company's physical address.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPhysicalAddress.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtEmployerRep.Text))
            {
                MessageBox.Show("Please enter the name of the company's represantative.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmployerRep.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtDesignation.Text))
            {
                MessageBox.Show("Please enter the company representative's designation.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtDesignation.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtInsurableEarnings.Text))
            {
                MessageBox.Show("Please enter the value of Insurable Earnings.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtInsurableEarnings.Focus();
                return false;
            }
            int outParse;
            if (!int.TryParse(txtInsurableEarnings.Text, out outParse))
            {
                MessageBox.Show("Please enter a numeric value for Insurable Earnings.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtInsurableEarnings.Focus();
                txtInsurableEarnings.SelectAll();
                return false;
            }
            if (Convert.ToInt32(txtInsurableEarnings.Text) < 0)
            {
                MessageBox.Show("Please enter a positive value for Insurable Earnings.", "Required Input Missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtInsurableEarnings.Focus();
                txtInsurableEarnings.SelectAll();
                return false;
            }
            return true;
        }

        private bool UpdateCompanyDetails()
        {
            theMainForm.SetBusyStatus();
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);               
                config.AppSettings.Settings["CompanyName"].Value = txtCompanyName.Text;
                config.AppSettings.Settings["SSRNo"].Value = txtSSRNo.Text;
                config.AppSettings.Settings["ECNo"].Value = txtECNo.Text;
                config.AppSettings.Settings["ICNo"].Value = txtICNo.Text;
                config.AppSettings.Settings["Sector"].Value = txtSector.Text;
                config.AppSettings.Settings["email"].Value = txtEmail.Text;
                config.AppSettings.Settings["Phone"].Value = txtPhone.Text;
                config.AppSettings.Settings["Address"].Value = txtPhysicalAddress.Text;
                config.AppSettings.Settings["EmployerRep"].Value = txtEmployerRep.Text;
                config.AppSettings.Settings["Designation"].Value = txtDesignation.Text;
                config.AppSettings.Settings["InsurableEarnings"].Value = txtInsurableEarnings.Text;

                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");

                theMainForm.SetReadyStatus();
                MessageBox.Show("Settings successfully saved.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;
            }
            catch(Exception ex)
            {
                theMainForm.SetReadyStatus();
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        #endregion

    }
}
