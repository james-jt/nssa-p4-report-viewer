﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

namespace P4ReportViewer
{
    public partial class frmHelp : Form
    {
        public frmHelp()
        {
            InitializeComponent();
            try
            {
                this.wBrHelp.Url = new Uri(Directory.GetCurrentDirectory() +  ConfigurationManager.AppSettings["HelpUrl"].ToString(), UriKind.RelativeOrAbsolute);
                this.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
