﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using P4Extractor;
using System.Configuration;
using SpreadsheetLight;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;

namespace P4ReportViewer
{
    public partial class frmMain : Form
    {
        DataTable tablePayrollTypes;
        DataTable tablePayrollNumbers;
        DataTable tableP4Data;
        DateTime payDate;
        string payrollMonthAndYear;
        string PayrollType;
        string PayrollNumber;
        int NpsInsurableEarnings;

        public frmMain()
        {
            InitializeComponent();
            RefreshCompanyDetails();
            RefreshMappingsCollection();
            DisplayDatabaseMapping();
            PopulatePayrollTypesDropDownList();
            payDate = new DateTime();
            payrollMonthAndYear = string.Empty;
            PayrollType = string.Empty;
            PayrollNumber = string.Empty;
        } 

        private void cmbPayrollType_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdatePayrollNumbersDropDownList();
            ClearReportDataFields();
        }

        private void cmbPayrollNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReportDataFields();
        }

        private void cmbPayMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReportDataFields();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            SetBusyStatus();
            if (string.IsNullOrEmpty(cmbPayrollType.Text) || string.IsNullOrEmpty(cmbPayrollNumber.Text))
            {
                SetReadyStatus();
                return;
            }
            tableP4Data = PrepareReport();
            this.dgvP4Report.DataSource = tableP4Data;
            FormatDataGridView();
            SetReadyStatus();
        }

        private void btnViewAll_Click(object sender, EventArgs e)
        {
            SetBusyStatus();
            if (string.IsNullOrEmpty(cmbPayMonth.Text))
            {
                SetReadyStatus();
                return;
            }
            tableP4Data = PrepareReportAllPayrolls();
            this.dgvP4Report.DataSource = tableP4Data;
            FormatDataGridView();
            SetReadyStatus();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            ExportReport();
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportReport();
        }

        private void selectDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ChangeDatabase())
            {
                MessageBox.Show("Database successfully changed.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                PopulatePayrollTypesDropDownList();
                if(tableP4Data != null)
                {
                    ClearReportDataFields();
                    dgvP4Report.DataSource = tableP4Data;
                }                              
            }            
        }

        private void selectMappingDynamicItem_Click(object sender, EventArgs e)
        {
            ToolStripItem clickedItem = (ToolStripItem)sender;
            string activeMapping = string.Empty;
            try
            {
                activeMapping = ConfigurationManager.AppSettings["ActiveMapping"];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
            if(clickedItem.Text == activeMapping)
            {
                return;
            }
            string msg = string.Format("Are you sure you want to change the database mapping to {0}", clickedItem.Text);
            if (MessageBox.Show(msg, "Confirm Mapping Change", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
            {
                return;
            }
            if (ChangeMapping(clickedItem))
            {
                MessageBox.Show(string.Format("Database mapping successfully changed to {0}.", clickedItem.Name), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                PopulatePayrollTypesDropDownList();
                if (tableP4Data != null)
                {
                    ClearReportDataFields();
                    dgvP4Report.DataSource = tableP4Data;
                }
            }
        }

        private void newMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNewMapping newMappingForm = new frmNewMapping(this);
            newMappingForm.ShowDialog();
            RefreshMappingsCollection();
            DisplayDatabaseMapping();
            PopulatePayrollTypesDropDownList();
            if (tableP4Data != null)
            {
                ClearReportDataFields();
                dgvP4Report.DataSource = tableP4Data;
            }
        }

        private void captureNatureOfEmploymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEmployeeDetails captureNatureofEmploymentForm = new frmEmployeeDetails(this);
        }

        private void editCompanyDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCompanyDetails editCompantDetailsForm = new frmCompanyDetails(this);
            RefreshCompanyDetails();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new abtAppAbout().ShowDialog();
        }

        private void viewHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmHelp();
        }


        #region Helper Methods

        private void RefreshCompanyDetails()
        {
            try
            {
                lblCompanyName.Text = ConfigurationManager.AppSettings["CompanyName"];
                txtSSRNo.Text = ConfigurationManager.AppSettings["SSRNo"];
                txtECNo.Text = ConfigurationManager.AppSettings["ECNo"];
                txtICNo.Text = ConfigurationManager.AppSettings["ICNo"];
                txtSector.Text = ConfigurationManager.AppSettings["Sector"];
                txtEmail.Text = ConfigurationManager.AppSettings["email"];
                txtPhone.Text = ConfigurationManager.AppSettings["Phone"];
                txtPhysicalAddress.Text = ConfigurationManager.AppSettings["Address"];
                txtEmployerRep.Text = ConfigurationManager.AppSettings["EmployerRep"];
                txtDesignation.Text = ConfigurationManager.AppSettings["Designation"];
                NpsInsurableEarnings = Convert.ToInt32(ConfigurationManager.AppSettings["InsurableEarnings"]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PopulatePayrollTypesDropDownList()
        {
            if (tablePayrollTypes != null)
            {
                tablePayrollTypes.Clear();
            }
            DataSet ds = (new DataAccess().ObtainPayrollTypes());
            if (ds.Tables.Count > 0)
            {
                tablePayrollTypes = ds.Tables[0];
                this.cmbPayrollType.DataSource = tablePayrollTypes;
                this.cmbPayrollType.DisplayMember = "CODE";
                this.cmbPayrollType.ValueMember = "CODE";

                UpdatePayrollNumbersDropDownList();
            }
        }

        private void UpdatePayrollNumbersDropDownList()
        {
            if (tablePayrollNumbers != null)
            {
                tablePayrollNumbers.Clear();
            }
            if (cmbPayrollType.SelectedValue == null)
            {
                return;
            }
            string payrollType = cmbPayrollType.SelectedValue.ToString();
            cmbPayrollNumber.Text = string.Empty;
            DataSet ds = (new DataAccess().ObtainPayrollNumbers(payrollType));
            if (ds.Tables.Count > 0)
            {
                tablePayrollNumbers = ds.Tables[0];
                this.cmbPayrollNumber.DataSource = tablePayrollNumbers;
                this.cmbPayrollNumber.ValueMember = "PAYROLLNUMBER";
                this.cmbPayrollNumber.DisplayMember = "PAYROLLNUMBER"; 
            }
        }

        private void DisplayDatabaseMapping()
        {
            try
            {
                string activeMapping = ConfigurationManager.AppSettings["ActiveMapping"];
                string databasePath = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                databasePath = databasePath.Substring(databasePath.IndexOf('=') + 1);
                databasePath = databasePath.Remove(databasePath.IndexOf(';')).Trim();
                lblMappingInfo.Text = string.Format("{0} [{1}]", activeMapping, databasePath).Trim();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearReportDataFields()
        {
            if (tableP4Data != null)
            {
                tableP4Data.Clear();
                tableP4Data = null;
            }
            txtPayDate.Clear();
            txtMonthAndYear.Clear();
            PayrollNumber = "";
            PayrollType = "";
        }

        private void RefreshMappingsCollection()
        {
            if (selectMappingToolStripMenuItem.DropDownItems.Count > 0)
            {
                selectMappingToolStripMenuItem.DropDownItems.Clear();
            }
            string activeMapping = string.Empty;
            try
            {
                activeMapping = '^' + ConfigurationManager.AppSettings["ActiveMapping"];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            foreach (ConnectionStringSettings mapping in ConfigurationManager.ConnectionStrings)
            {
                if (mapping.Name.StartsWith("^"))
                {
                    ToolStripMenuItem mappingListItem = new ToolStripMenuItem();
                    mappingListItem.Text = mappingListItem.Name = mapping.Name.TrimStart('^');
                    mappingListItem.Click += new EventHandler(selectMappingDynamicItem_Click);
                    if (mapping.Name == activeMapping)
                    {
                        mappingListItem.Font = new System.Drawing.Font(mappingListItem.Font, FontStyle.Bold);
                    }                    
                    selectMappingToolStripMenuItem.DropDownItems.Add(mappingListItem);                    
                }
            }
        }

        private bool ChangeDatabase()
        {
            var fileOpenDialogue = new OpenFileDialog();
            fileOpenDialogue.Filter = "Golden Pay database file (Executive.sci)|Executive.sci|All Soft-IT database files (*.sci)|*.sci";
            fileOpenDialogue.ShowDialog();
            if (string.IsNullOrEmpty(fileOpenDialogue.FileName))
            {
                return false;
            }
            string databaseFile = fileOpenDialogue.FileName.ToString();

            string newConnectionString = string.Format(@"database = localhost:{0};
            User = SYSDBA; Password = masterkey; DataSource=localhost; Port = 3050; Dialect = 3; Charset = NONE;
            Role =; Connection lifetime = 15; Pooling = true; MinPoolSize = 0; MaxPoolSize = 50; Packet Size = 8192; ServerType = 0", databaseFile);
            if (newConnectionString.Equals(new DataAccess().connectionString))
            {
                return false;
            }
            SetBusyStatus();
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["DefaultConnection"].ConnectionString = newConnectionString;
                config.AppSettings.Settings["ActiveMapping"].Value = "";
                config.Save(ConfigurationSaveMode.Modified, true);
                ConfigurationManager.RefreshSection("connectionStrings");
                ConfigurationManager.RefreshSection("appSettings");

                RefreshMappingsCollection();
                DisplayDatabaseMapping();
                SetReadyStatus();
                return true;
            }
            catch (Exception ex)
            {
                SetReadyStatus();
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }   
        }

        private bool ChangeMapping (ToolStripItem clickedItem)
        {
            string selectedMappingConnectionString = string.Empty;
            //Add back the '^' character to the mapping name before reading config file.
            string selectedMapping = '^' + clickedItem.Text;
            SetBusyStatus();
            try
            {
                selectedMappingConnectionString = ConfigurationManager.ConnectionStrings[selectedMapping].ConnectionString;
            }
            catch (Exception ex)
            {
                SetReadyStatus();
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["DefaultConnection"].ConnectionString = selectedMappingConnectionString;
                config.AppSettings.Settings["ActiveMapping"].Value = selectedMapping.TrimStart('^');
                config.Save(ConfigurationSaveMode.Modified, true);
                ConfigurationManager.RefreshSection("connectionStrings");
                ConfigurationManager.RefreshSection("appSettings");

                RefreshMappingsCollection();
                DisplayDatabaseMapping();
                SetReadyStatus();
                return true;        
            }
            catch (Exception ex)
            {
                SetReadyStatus();
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private DataTable PrepareReport()
        {
            DataTable dt = new DataTable();
            PayrollType = cmbPayrollType.Text.ToString();
            PayrollNumber = cmbPayrollNumber.Text.ToString();
            string NssaDeductionCode = string.Empty;
            try
            {
                NssaDeductionCode = ConfigurationManager.AppSettings["NssaDeductionCode"];
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            DataSet ds = new DataAccess().GetReportData(PayrollType, PayrollNumber, NssaDeductionCode);
            if (ds.Tables.Count > 0)
            {
                dt = FormatReport(ds);
                txtMonthAndYear.Text = payrollMonthAndYear = GetPayrollMonthAndYear();
                GetPayrollPayDate();
            }
            return dt;        
        }

        private DataTable PrepareReportAllPayrolls()
        {
            DataTable dt = new DataTable();
            string payrollMonth = cmbPayMonth.SelectedIndex.ToString();
            string NssaDeductionCode = string.Empty;
            try
            {
                NssaDeductionCode = ConfigurationManager.AppSettings["NssaDeductionCode"];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            DataSet ds = new DataAccess().GetCombinedReportData(payrollMonth, NssaDeductionCode);
            if (ds.Tables.Count > 0)
            {
                dt = FormatReport(ds);
                txtMonthAndYear.Text = payrollMonthAndYear = cmbPayMonth.Text + ", " + GetFinancialYear();
                txtPayDate.Text = " - ";
            }
            return dt;
        }

        private DataTable FormatReport(DataSet ds)
        {
            DataTable dt = new DataTable();
            dt = ds.Tables[0];
            dt.Columns[0].ColumnName = "SSN";
            dt.Columns[1].ColumnName = "Employee Staff No.";
            dt.Columns[2].ColumnName = "National ID No.";
            dt.Columns[3].ColumnName = "Date of Birth";
            dt.Columns[4].ColumnName = "Surname";
            dt.Columns[5].ColumnName = "First Name(s)";
            dt.Columns[6].ColumnName = "Commencement Date";
            dt.Columns[7].ColumnName = "Cessation Date";
            dt.Columns[8].ColumnName = "Reason for Cessation";
            dt.Columns[9].ColumnName = "Nature of Employment";
            dt.Columns[10].ColumnName = "NPS Insurable Earnings";
            dt.Columns[11].ColumnName = "Total NPS (7%) Contribution this Month";
            dt.Columns[12].ColumnName = "Basic Salary (WCIF) Ex. Allowances";

            for (int row = 0; row < dt.Rows.Count; row++)
            {
                DataRow currentEmployee = dt.Rows[row];
                switch (currentEmployee["Reason for Cessation"].ToString())
                {
                    case "**N/D**":
                        currentEmployee["Reason for Cessation"] = "";
                        break;
                    case "1":
                        currentEmployee["Reason for Cessation"] = "O";
                        break;
                    case "2":
                        currentEmployee["Reason for Cessation"] = "O";
                        break;
                    case "3":
                        currentEmployee["Reason for Cessation"] = "O";
                        break;
                    case "4":
                        currentEmployee["Reason for Cessation"] = "C";
                        break;
                    case "5":
                        currentEmployee["Reason for Cessation"] = "R";
                        break;
                    case "6":
                        currentEmployee["Reason for Cessation"] = "D";
                        break;
                    default:
                        currentEmployee["Reason for Cessation"] = "O";
                        break;
                }
                if (Convert.ToInt32(currentEmployee["Basic Salary (WCIF) Ex. Allowances"]) > NpsInsurableEarnings)
                {
                    currentEmployee["NPS Insurable Earnings"] = NpsInsurableEarnings;
                }
                else
                {
                    currentEmployee["NPS Insurable Earnings"] = currentEmployee["Basic Salary (WCIF) Ex. Allowances"];
                }
            }
            return dt;
        }

        private void FormatDataGridView()
        {

            this.dgvP4Report.AdvancedColumnHeadersBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.Single;
            this.dgvP4Report.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font(this.dgvP4Report.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            DataGridViewCellStyle style = new DataGridViewCellStyle();
            style.Format = "#,##0.00";
            this.dgvP4Report.Columns[10].DefaultCellStyle = style;
            this.dgvP4Report.Columns[11].DefaultCellStyle = style;
            this.dgvP4Report.Columns[12].DefaultCellStyle = style;
        }

        private bool ExportReport()
        {
            if(tableP4Data == null)
            {
                MessageBox.Show("No data selected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            var fileSaveDialogue = new SaveFileDialog();
            fileSaveDialogue.Filter = "Microsoft Office Xml Excel (*.xlsx)|*.xlsx"; 
            fileSaveDialogue.ShowDialog();
            if (string.IsNullOrEmpty(fileSaveDialogue.FileName))
            {
                return false;
            }
            string outputFilePath = fileSaveDialogue.FileName.ToString();

            SetBusyStatus();
            SLDocument Wbook = PrepareWorkbook();

            if (outputFilePath.EndsWith(".xlsx"))
            {
                try
                {
                    Wbook.SaveAs(outputFilePath);
                    SetReadyStatus();
                    if(MessageBox.Show("File successfully saved. Do you wish to open this file now?", "Success", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        SetBusyStatus();
                        System.Diagnostics.Process.Start(outputFilePath);
                        SetReadyStatus();
                    }
                }
                catch(Exception ex)
                {
                    SetReadyStatus();
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                SetReadyStatus();
                MessageBox.Show("Invalid file extension.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private SLDocument PrepareWorkbook()
        {
            SLDocument wBook = new SLDocument();
            wBook.RenameWorksheet("Sheet1", PayrollType + " - " + PayrollNumber);
            int iStartRowIndex = 10;
            int iStartColumnIndex = 1;
            wBook.ImportDataTable(iStartRowIndex, iStartColumnIndex, tableP4Data, true);

            SLPageSettings pageSettings = new SLPageSettings();
            pageSettings.Orientation = OrientationValues.Landscape;
            pageSettings.SetTabColor(SLThemeColorIndexValues.Accent1Color);
            wBook.SetPageSettings(pageSettings);
            
            //Format columns
            SLStyle style = wBook.CreateStyle();
            style = wBook.GetColumnStyle(11); //Check effect
            style.FormatCode = "dd/mm/yyyy";
            wBook.SetColumnStyle(4, style);
            wBook.SetColumnStyle(7, style);
            wBook.SetColumnStyle(8, style); 

            style.FormatCode = "#,##0.00";
            wBook.SetColumnStyle(11, style);
            wBook.SetColumnStyle(12, style);
            wBook.SetColumnStyle(13, style);

            wBook.AutoFitColumn(0, 13);

            //Create and add table
            int iEndRowIndex = iStartRowIndex + tableP4Data.Rows.Count + 1 - 1;
            int iEndColumnIndex = iStartColumnIndex + tableP4Data.Columns.Count - 1;
            SLTable table = wBook.CreateTable(iStartRowIndex, iStartColumnIndex, iEndRowIndex, iEndColumnIndex);
            table.SetTableStyle(SLTableStyleTypeValues.Light1);
            table.HasTotalRow = true;
            table.SetTotalRowFunction(11, SLTotalsRowFunctionValues.Sum);
            table.SetTotalRowFunction(12, SLTotalsRowFunctionValues.Sum);
            wBook.InsertTable(table);
             
            SLStyle cellStyle = wBook.CreateStyle();
            cellStyle = wBook.GetCellStyle(3, 1); //Check effect
            cellStyle.SetFontBold(true);
            wBook.SetCellStyle(3, 1, cellStyle);
            wBook.SetCellStyle(4, 1, cellStyle);
            wBook.SetCellStyle(5, 1, cellStyle);
            wBook.SetCellStyle(6, 1, cellStyle);
            wBook.SetCellStyle(7, 1, cellStyle);
            wBook.SetCellStyle(8, 1, cellStyle);
            wBook.SetCellStyle(3, 4, cellStyle);
            wBook.SetCellStyle(4, 4, cellStyle);
            wBook.SetCellStyle(5, 4, cellStyle);
            wBook.SetCellStyle(6, 4, cellStyle);
            wBook.SetCellStyle(iEndRowIndex + 1, 1, cellStyle);
            wBook.SetCellStyle(iEndRowIndex + 1, 2, cellStyle);
            wBook.SetCellStyle(iEndRowIndex + 3, 1, cellStyle);
            wBook.SetCellStyle(iEndRowIndex + 4, 1, cellStyle);
            wBook.SetCellStyle(iEndRowIndex + 5, 1, cellStyle);

            cellStyle.SetFont("Calibri",12);
            wBook.SetCellStyle(1, 1, cellStyle);

            style.FormatCode = "dd/mm/yyyy";
            style.SetHorizontalAlignment(HorizontalAlignmentValues.Left);  
            wBook.SetCellStyle(iEndRowIndex + 5, 2, style);

            //Add company details
            wBook.SetCellValue(3, 1, "Employer SSR No. : ");
            wBook.SetCellValue(4, 1, "Employer EC No. : ");
            wBook.SetCellValue(5, 1, "IC : ");
            wBook.SetCellValue(6, 1, "Sector : ");
            wBook.SetCellValue(7, 1, "Employer's Name : ");
            wBook.SetCellValue(8, 1, "Address : ");
            wBook.AutoFitColumn(1, 1);

            wBook.SetCellValue(3, 4, "Payment Month and Year : ");
            wBook.SetCellValue(4, 4, "Email : ");
            wBook.SetCellValue(5, 4, "Tel : ");
            wBook.AutoFitColumn(4, 4);
            
            wBook.SetCellValue(1, 1, "Monthly Payment Schedule of Employees (Form P4)");
            wBook.SetCellValue(iEndRowIndex + 3, 1, "Employer Rep : ");
            wBook.SetCellValue(iEndRowIndex + 4, 1, "Designation : ");
            wBook.SetCellValue(iEndRowIndex + 5, 1, "Date : ");

            wBook.SetCellValue(3, 2, txtSSRNo.Text);
            wBook.SetCellValue(4, 2, txtECNo.Text);
            wBook.SetCellValue(5, 2, txtICNo.Text);
            wBook.SetCellValue(6, 2, txtSector.Text);
            wBook.SetCellValue(7, 2, lblCompanyName.Text);
            wBook.SetCellValue(8, 2, txtPhysicalAddress.Text);

            wBook.SetCellValue(3, 5, payrollMonthAndYear);
            wBook.SetCellValue(4, 5, txtEmail.Text);
            wBook.SetCellValue(5, 5, txtPhone.Text);

            wBook.SetCellValue(iEndRowIndex + 3, 2, txtEmployerRep.Text);
            wBook.SetCellValue(iEndRowIndex + 4, 2, txtDesignation.Text);
            wBook.SetCellValue(iEndRowIndex + 5, 2, DateTime.Today);


            wBook.SetCellValue(iEndRowIndex + 1, 1, "TOTALS:");
            wBook.SetCellValue(iEndRowIndex + 1, 2, string.Format("{0} Staff Members", tableP4Data.Rows.Count.ToString()));

            return wBook;
        }

        private string FormatMonth(int monthNumber)
        {
            switch (monthNumber)
            {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
                default:
                    return "N/D";
            }
        }

        private string GetPayrollMonthAndYear()
        {
            string payrollMonthAndYear = string.Empty;

            DataSet ds = new DataAccess().ObtainPayrollMonthAndYear(PayrollNumber);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    payrollMonthAndYear = string.Format(FormatMonth((int)ds.Tables[0].Rows[0][0]) + ", " + ds.Tables[0].Rows[0][1].ToString());
                }
            }
            return payrollMonthAndYear;
        }

        private void GetPayrollPayDate()
        {
            DataSet dst = new DataAccess().ObtainPayDate(PayrollType, PayrollNumber);
            if (dst.Tables.Count > 0)
            {
                if (dst.Tables[0].Rows.Count > 0)
                {
                    DateTime.TryParse(dst.Tables[0].Rows[0][0].ToString(), out payDate);
                }
            }
            txtPayDate.Text = payDate.DayOfWeek + " " + payDate.Day + " " + FormatMonth(payDate.Month) + ", " + payDate.Year.ToString();
        }

        private string GetFinancialYear()
        {
            string payrollFinancialYear = string.Empty;

            DataSet ds = new DataAccess().ObtainFinancialYear();
            if (ds.Tables.Count > 0) 
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    payrollFinancialYear = ds.Tables[0].Rows[0][0].ToString();
                }
            }
            return payrollFinancialYear;
        }

        public void SetBusyStatus()
        {
            lblStatus.Text = "Busy...";
            picBusyStatus.Visible = true;
            picReadyStatus.Visible = false;
            this.Cursor = Cursors.WaitCursor;
            Application.DoEvents();
        }

        public void SetReadyStatus()
        {
            lblStatus.Text = "Ready...";
            picBusyStatus.Visible = false;
            picReadyStatus.Visible = true;
            this.Cursor = Cursors.Default;
            Application.DoEvents();
        }

        #endregion

    }
}
