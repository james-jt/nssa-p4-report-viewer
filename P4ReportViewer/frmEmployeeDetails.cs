﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using P4Extractor;

namespace P4ReportViewer
{
    public partial class frmEmployeeDetails : Form
    {
        DataSet ds;
        DataTable tableEmployees;
        frmMain theMainForm;
        public frmEmployeeDetails(frmMain mainForm)
        {
            InitializeComponent();
            theMainForm = mainForm;

            ds = new DataAccess().GetAllEmployees();
            if (ds.Tables.Count > 0)
            {
                tableEmployees = FormatEmployeesList();
                this.dgvAllEmployees.DataSource = tableEmployees;
                this.dgvAllEmployees.AdvancedColumnHeadersBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.Single;
                this.dgvAllEmployees.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font(this.dgvAllEmployees.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);

                DataGridViewComboBoxColumn cmbColumn = new DataGridViewComboBoxColumn();
                cmbColumn.Name = "New Nature of Employment";
                cmbColumn.MaxDropDownItems = 2;
                cmbColumn.Items.Add("A");
                cmbColumn.Items.Add("N");
                cmbColumn.FlatStyle = FlatStyle.Popup;
                

                dgvAllEmployees.Columns.Add(cmbColumn);
                this.ShowDialog();
            }
            else
            {
                string msg = @"Could not retrieve employee records.";
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            if (SaveAllRecords())
            {
                this.Close();
            }
        }

        private void btnSaveAndContinue_Click(object sender, EventArgs e)
        {
            SaveAllRecords();
        }

        #region Helper Methods

        private DataTable FormatEmployeesList()
        {
            DataTable dt = ds.Tables[0];
            dt.Columns[0].ColumnName = "Employee Number";
            dt.Columns[1].ColumnName = "Date of Birth";
            dt.Columns[2].ColumnName = "Surname";
            dt.Columns[3].ColumnName = "Name";
            dt.Columns[4].ColumnName = "Current Nature of Employment";
            dt.Columns[0].ReadOnly = true;
            dt.Columns[1].ReadOnly = true;
            dt.Columns[2].ReadOnly = true;
            dt.Columns[3].ReadOnly = true;
            return dt;
        }

        private bool SaveAllRecords ()
        {
            theMainForm.SetBusyStatus();
            for (int row = 0; row < dgvAllEmployees.Rows.Count; row++)
            {
                if(dgvAllEmployees.Rows[row].Cells["New Nature of Employment"].Value != null)
                {
                    tableEmployees.Rows[row]["Current Nature of Employment"] = dgvAllEmployees.Rows[row].Cells["New Nature of Employment"].Value;
                }
            }
            dgvAllEmployees.DataSource = tableEmployees;

            if (new DataAccess().UpdateEmployeesNOE(tableEmployees))
            {
                theMainForm.SetReadyStatus();
               MessageBox.Show("Records successfully updated.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
               return true;
            }
            else
            {
                theMainForm.SetReadyStatus();
                MessageBox.Show("Some or all records might not have been successfully updated.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        #endregion

    }
}
